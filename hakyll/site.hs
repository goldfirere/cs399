-- Generate main course site
-- Copyright (c) Richard Eisenberg

{-# LANGUAGE OverloadedStrings, TypeApplications, ViewPatterns, GeneralizedNewtypeDeriving,
             ScopedTypeVariables #-}

module Main where

import Hakyll
import Control.Monad
import Data.Monoid
import System.FilePath
import Text.Printf
import Data.List
import Debug.Trace
import Text.Pandoc ( Pandoc(..), Meta(..), MetaValue(..), Inline(..) )
import qualified Data.Map as M
import Data.Coerce
import Data.Binary
import System.Process

main :: IO ()
main = hakyll $ do
  piimatch "placement.html" $ do
    route   idRoute
    compile copyFileCompiler

  piimatch "web/css/*" $ do
    route   dropWebRoute
    compile compressCssCompiler

  piimatch "web/images/*" $ do
    route   dropWebRoute
    compile copyFileCompiler

  piimatch (fromRegex "^project/[^/]*\\.(pdf|java)") $ do
    route   idRoute
    compile copyFileCompiler

  piimatch "**.md" $ do   -- catchall
    route   $ setExtension "html"
    compile $ mdCompiler

  piimatch "web/templates/*.html" $ compile templateBodyCompiler

-- drop a "web/" prefix
dropWebRoute :: Routes
dropWebRoute = pathRoute tail

-- defaultContext + basename
wrapperContext :: Context String
wrapperContext =
  defaultContext <>
  field "basename" (return . takeBaseName . toFilePath . itemIdentifier)

-- do all the processing I expect on an md file
mdCompiler :: Compiler (Item String)
mdCompiler = do
  body <- getResourceString
  pandoc <- readPandoc body
  let html = writePandoc pandoc
  substed <- applyAsTemplate mempty html
  full_page <- loadAndApplyTemplate "web/templates/wrapper.html" wrapperContext substed
  relativizeUrls full_page

------------------
-- a customRoute that alters the list of filepath components. Directories in
-- this list end with '/'
pathRoute :: ([FilePath] -> [FilePath]) -> Routes
pathRoute f = customRoute $ joinPath . f . splitPath . toFilePath

piimatch :: Pattern -> Rules () -> Rules ()
piimatch pat = match (pat .&&. complement "pii/**" .&&. complement "private/**")
