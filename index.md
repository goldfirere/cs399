---
title: "CS 399: Senior Conference"
---

<div id="header">

| **CS 399: Senior Conference**
| Prof. Richard Eisenberg
| Spring 2019
| Bryn Mawr College

</div>

General information
===================

<div id="info_table">

----------------------         -----------------------------------------------------------------------------------------------------------------------------------------
Instructor:                    [Richard Eisenberg](http://cs.brynmawr.edu/~rae)
Email:                         `rae@cs.brynmawr.edu`
Office Phone:                  610-526-5061
Home Phone (emergencies only): 484-344-5924
Cell Phone (emergencies only): 201-575-6474 (no texts, please)
Office:                        Park 204
Office Hours:                  Wednesdays 2:30-4:00pm; Fridays 1:00-2:00pm
                               If these don't work, do not fret. Email instead.
<span class="strut" />
Class meeting:                 F 2:10-4:00pm in Park 227
Website:                       <http://cs.brynmawr.edu/cs399>
GitLab Repo:                   <https://gitlab.com/goldfirere/cs399>
Piazza Q&A Forum:              <https://piazza.com/brynmawr/spring2019/cs399/home>
----------------------         -----------------------------------------------------------------------------------------------------------------------------------------

</div>

Goals of course
---------------

<div id="goals">

By the end of this course, you will have...

* completed a substantial project or thesis in the field of computer science
* written a technical paper on your work
* critiqued others' technical writing
* givens and critiqued computer science presentations

</div>

This course establishes the meeting time for all seniors in computer science
to support each other during the creation of senior theses and projects.
We will meet weekly to offer peer feedback, talk about the thesis/project process,
and discuss technical writing.

Deadlines
---------

<div class="deadline_table">

-----------------------    ---------------------------------------------------------------------
Thursday, Feb. 7           Three-page annotated outline
Thursday, Feb. 21          Extended abstract (8-10 pages):
                           intro, motivation, background, literature review, preliminary results
                           *or*
                           Working prototype (for projects)
Thursday, Mar. 7           First draft
Thursday, Apr. 4           Second draft; working implementation (if applicable)
Thursday, Apr. 18          Final draft; all code to be submitted
Thursday, Apr. 25          Thesis/project writeup due (firm!)
During finals period       Senior presentations
-----------------------    ---------------------------------------------------------------------

</div>

Grading
-------

<div class="grading_table">

-----------------------------------------------   --------------
Attendance                                         20%
Making all deadlines                               20%
Discussions, progress reports, peer feedback       20%
Thesis/project quality                             40%
<span class="strut" />
Total                                              100%
-----------------------------------------------   --------------

</div>

Assigned project
================

Seniors not completing a thesis or independent project will be writing
a Lisp interpreter.

* [Project description](project/Project.pdf)
* [ANSI Common Lisp](http://www.paulgraham.com/acl.html), by Paul Graham
* [Common Lisp: The Language, 2nd Edition](https://www.cs.cmu.edu/Groups/AI/html/cltl/cltl2.html), by Guy L. Steele
* [Common Lisp HyperSpec](http://www.lispworks.com/documentation/HyperSpec/Front/), the language specification
* [My test file](project/Eval.java)
